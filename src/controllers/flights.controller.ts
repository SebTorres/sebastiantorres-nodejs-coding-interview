import { JsonController, Get } from 'routing-controllers'
import { FlightsService } from '../services/flights.service'
import {classToPlain } from 'class-transformer'

const flightsService = new FlightsService()

@JsonController('/flights')
export default class FlightsController {
    @Get('', { transformResponse: true })
    async getAll() {
        return {
            status: 200,
            data: flightsService.getAll(),
        }
    }
}
